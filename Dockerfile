FROM node:14-alpine

COPY dist ./dist
COPY package*.json ./
RUN npm install

CMD ["node", "./dist/server.js"]

EXPOSE 80
