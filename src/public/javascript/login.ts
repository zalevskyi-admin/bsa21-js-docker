const username = sessionStorage.getItem('username');

if (username) {
  window.location.replace('/game');
}

const submitButton = document.getElementById('submit-button');
const input = document.getElementById(
  'username-input',
) as HTMLInputElement | null;

//const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  //const inputValue = getInputValue();
  if (input) {
    if (input.value) {
      sessionStorage.setItem('username', input.value);
      window.location.replace('/game');
    }
  }
};

const onKeyUp = (ev: KeyboardEvent) => {
  if (ev.key === 'Enter' && submitButton) {
    submitButton.click();
  }
};

if (submitButton) {
  submitButton.addEventListener('click', onClickSubmitButton);
}
window.addEventListener('keyup', onKeyUp);
