import { PlaygroundText, UserIndicator } from './gameComponents.js';
import { getSocket } from './socketTSNOCHECK.js';

const gamePage = document.getElementById('game-page') as HTMLSpanElement | null;
const userList = document.createElement('div') as HTMLDivElement;
const playground = document.createElement('div') as HTMLDivElement;
const readyButton = document.createElement('button') as HTMLButtonElement;
const playgroundTimer = document.createElement('div');
const playgroundText = new PlaygroundText();
const username = sessionStorage.getItem('username') || '';

if (username === '') {
  window.location.replace('/login');
}
const socket = getSocket(username);

if (gamePage) {
  gamePage.appendChild(userList);
  userList.classList.add('user-list');
  gamePage.appendChild(playground);
  playground.classList.add('playground', 'flex-centered', 'no-select');
  playground.appendChild(readyButton);
  playground.appendChild(playgroundText.element);
  playground.appendChild(playgroundTimer);
  playgroundTimer.classList.add('display-none');
  playgroundText.element.classList.add('display-none');
  readyButton.addEventListener('click', changeStatus);
  readyButton.classList.add('ready-button', 'status-ready');
  readyButton.innerText = 'Ready';
}

function check({ key }: KeyboardEvent) {
  if (key.length === 1) {
    socket.emit('TYPE', key);
  }
}

let ready = false;

function changeStatus() {
  if (ready) {
    ready = false;
    readyButton.innerText = 'Ready';
    readyButton.classList.add('status-ready');
    readyButton.classList.remove('status-notready');
  } else {
    ready = true;
    readyButton.innerText = 'Not Ready';
    readyButton.classList.add('status-notready');
    readyButton.classList.remove('status-ready');
  }
  socket.emit('READY', ready);
}

socket.on('TEXT_SELECTED', textId => {
  fetch(`game/text/${textId}`)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(responseJSON => {
      playgroundText.setText(responseJSON.data);
    });
});

socket.on('COUNTDOWN', start => {
  readyButton.classList.add('display-none');
  playgroundTimer.classList.remove('display-none');
  playgroundTimer.innerText = start;
  if (Number(start) === 0) {
    playgroundTimer.classList.add('display-none');
    playgroundText.element.classList.remove('display-none');
    document.addEventListener('keydown', check);
  }
});

socket.on('TYPE_RESULT', count => {
  if (Array.isArray(count)) {
    const doneCount = Number(count[0]);
    const totalCount = Number(count[1]);
    if (
      Number.isNaN(doneCount) === false &&
      Number.isNaN(totalCount) === false
    ) {
      if (playgroundText.getDoneCount() < doneCount) {
        playgroundText.setDone(doneCount);
        return;
      }
    }
  }
  playgroundText.showMistake();
});

socket.on('disconnect', () => {
  window.alert(`Username: ${username} is already in use. Choose another one.`);
  sessionStorage.removeItem('username');
  window.location.replace('/login');
});

socket.on('PLAYERS', players => {
  userList.innerHTML = '';
  if (Array.isArray(players)) {
    players.forEach(player => {
      if (Array.isArray(player)) {
        const me = player[0] === username ? true : false;
        const indicator = new UserIndicator(player[0], player[1], me);
        userList.appendChild(indicator.element);
        indicator.progressBar.setProgress(player[2] / player[3]);
      }
    });
  }
});
