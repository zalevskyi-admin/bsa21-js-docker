// @ts-nocheck
// It is trusted to be loaded via script in HTML
// <script src="/socket.io/socket.io.js"></script>
// tsc on its own does not provide ability to import socket.io-client in distribution code
// and this project is done only with tsc
import { Socket } from 'socket.io-client';
export function getSocket(username: string): Socket {
  return io('', { query: { username } });
}
